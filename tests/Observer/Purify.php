<?php
namespace Crux;

/**
 * @group crux
 */
class Test_Observer_Purify extends DatabaseTestCase
{
    public function testPurifyValidJson1()
    {
        $json = json_encode('abc');
    
        $this->assertEquals('"abc"', Observer_Purify::purifyJson(Observer_Purify::getPurifier(), $json));
    }
    
    public function testPurifyValidJson2()
    {
        $json = json_encode(array(
            'abc',
            'def',
            'hij'
        ));
    
        $this->assertEquals($json, Observer_Purify::purifyJson(Observer_Purify::getPurifier(), $json));
    }
    
    public function testPurifyValidJson3()
    {
        $json = json_encode(array(
            'abc' => 'def',
            1 => 'hij'
        ));
    
        $this->assertEquals($json, Observer_Purify::purifyJson(Observer_Purify::getPurifier(), $json));
    }
    
    public function testPurifyValidJson4()
    {
        $json = json_encode('<script type="text/javascript">alert(\'hoi\');</script>');
    
        $this->assertEquals('""', Observer_Purify::purifyJson(Observer_Purify::getPurifier(), $json));
    }
    
    public function testPurifyValidJson5()
    {
        $json = json_encode(array(
            1,
            '<script type="text/javascript">alert(\'hoi\');</script>'
        ));
    
        $this->assertEquals('["1",""]', Observer_Purify::purifyJson(Observer_Purify::getPurifier(), $json));
    }
    
    public function testPurifyValidJson6()
    {
        $json = json_encode(array(
            1 => '<script type="text/javascript">alert(\'hoi\');</script>',
            '<script type="text/javascript">alert(\'hoi\');</script>' => 1
        ));
    
        $this->assertEquals('{"1":"","":"1"}', Observer_Purify::purifyJson(Observer_Purify::getPurifier(), $json));
    }
    
    public function testPurifyInvalidJson()
    {
        $json = 'hjqhduihqhadsjh';
    
        $this->assertEquals('""', Observer_Purify::purifyJson(Observer_Purify::getPurifier(), $json));
    }
    
	function testScriptRemoval()
	{
		$props = array(
			'test' => '<script>alert(\'hoi\');</script>',
		);
		
		$model = Model::forge($props);
		
		$model->save();
		
		$this->assertEquals('', $model->test);
		$this->assertEquals($props['test'], $model->test_unpurified);
	}
	
	function testXSS()
	{
		$props = array(
			'test' => '\'\';!--"<XSS>=&{()}',
		);
		
		$model = Model::forge($props);
		
		$model->save();
		
		$this->assertEquals('\'\';!--"=&amp;{()}', $model->test);
		$this->assertEquals($props['test'], $model->test_unpurified);
	}
	
	
	function testUpdate()
	{
		$props = array(
			'test' => '\'\';!--"<XSS>=&{()}',
		);
		
		$model = Model::forge($props);
		
		$model->save();
		$model->test = '&';
		$model->save();
		
		$this->assertEquals('&amp;', $model->test);
		$this->assertEquals('&', $model->test_unpurified);
	}
}