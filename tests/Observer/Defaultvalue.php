<?php
namespace Crux;

/**
 * @group crux
 */
class Test_Observer_DefaultValue extends DatabaseTestCase
{
	function testDefaultValue()
	{
		$model = Model::forge();
		
		foreach($this->_properties as $prop)
		{
			$this->assertNull($model->{$prop});
		}
		
		$model->save();
		
		foreach($this->_properties as $prop)
		{
			$this->assertEquals('', $model->{$prop});
		}
	}
}