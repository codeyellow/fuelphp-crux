<?php
namespace Crux;

/**
 * @group crux
 */
class Test_Model extends DatabaseTestCase
{
	function testCreatedAt()
	{
		$model = Model::forge();
		$model->save();
		
		$this->assertNotEquals('', $model->created_at); 
	}
	
	function testUpdatedAt()
	{
		$model = Model::forge();
		$model->save();

		$this->assertNotEquals('', $model->updated_at);
		
		sleep(3);
		
		$model->test = 'update';
		$model->save();
		
		$this->assertNotEquals($model->created_at, $model->updated_at); 
	}
}