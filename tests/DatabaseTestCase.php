<?php
namespace Crux;

/**
 * @group crux
 */
abstract class DatabaseTestCase extends \TestCase
{
	protected $_properties = array('test', 'test_unpurified');
	
	function setUp()
	{
		\DBUtil::drop_table('models');
		
		\DBUtil::create_table(
			'models',
			array(
					'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
					'test' => array('type' => 'text'),
					'test_unpurified' => array('type' => 'text'),
					'created_at' => array('type' => 'int', 'constraint' => 11),
					'updated_at' => array('type' => 'int', 'constraint' => 11)
			),
			array('id')
		);
	}

    function tearDown()
    {
		\DBUtil::drop_table('models');
    }
}