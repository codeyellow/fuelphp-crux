<?php
/**
 * Purify observer.
 * 
 * TODO: Move htmlpurifier to myorm.
 *
 * @package		myorm
 * @version		1.0
 * @author		AB Zainuddin
 * @copyright	2012 Zaicorp BV
 * @link		http://www.zaicorp.nl
 */
namespace Crux;

class Observer_Purify extends \Orm\Observer
{
    private $_suffix = '_unpurified';
    protected static $purifier = null;
    
    public static function getPurifier() {
        if(!static::$purifier instanceof \HTMLPurifier) {
            $config = \HTMLPurifier_Config::createDefault()->set('HTML.Doctype', 'XHTML 1.0 Strict');
            static::$purifier = new \HTMLPurifier($config);
        } 
        
        return static::$purifier;
    }
    
    /**
     * Purify a json string. 
     *
     * @param \HtmlPurifier $purifier
     * @param \string $val
     * @return string If $val is an valid json string, it returns a purified json string, else "".
     */
    public static function purifyJson(\HTMLPurifier $purifier, $val)
    {
        $result = '';
    
        if (strlen($val) > 0) {
            $val = json_decode($val);
    
            // Check for json errors.
            if(json_last_error() == JSON_ERROR_NONE) {
                switch(true)
                {
                    case is_object($val):
                        foreach ($val as $aKey => $aVal) {
                            $result[$purifier->purify($aKey)] = $purifier->purify($aVal);
                        }
                        break;
                    case is_array($val):
                        foreach ($val as $aVal) {
                            $result[] = $purifier->purify($aVal);
                        }
                        break;
                    default:
                        $result = $purifier->purify($val);
                        break;
                }
            }
        }
    
        return json_encode($result);
    }

    /**
	 * Saves purified/unpurified data.
	 * 
	 * @param \Orm\Model $obj
	 */
    public function before_save(\Orm\Model $obj)
    {
        $pattern = '/(' . $this->_suffix . ')$/';
        $properties = $obj->properties();
        $changedProperties = $obj->changedProperties();
        $purifier = static::getPurifier();
        
        // Iterate all changed properties.
        foreach (array_keys($changedProperties) as $field) {
            $field_unpurifed = $field . $this->_suffix;
            
            // Check for _unpurified fields.
            if (array_key_exists($field_unpurifed, $properties)) {
                $val = $obj->{$field};
                $purified = $val;
                $unpurified = $val;
                
                // Only purify stuff that can be purified.
                switch(true) {
                    case array_key_exists('purify', $properties[$field]) && is_callable($properties[$field]['purify']):
                         $purified = call_user_func($properties[$field]['purify'], $purifier, $val);
                         $unpurified = $obj->{$field};
                        break;
                    case is_string($val):
                        $purified = $purifier->purify($val);
                        $unpurified = $obj->{$field};
                        break;
                    case is_array($val):
                        // TODO: Just do nothing now.
                        break;
                }
                
                $obj->{$field_unpurifed} = $unpurified;
                $obj->{$field} = $purified;
            }
        }
    }
}