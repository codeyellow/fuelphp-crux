<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package     myorm
 * @version     1.0
 * @author      AB Zainuddin
 * @copyright   2012 Zaicorp BV
 * @link        http://www.zaicorp.nl
 */

namespace Crux;

class Observer_DefaultValue extends \Orm\Observer
{
    private $_default_value = '';

    /**
     * Replace nulls with $_default_value. Hack for db error ... cannot be null.
     * 
     * @param \Orm\Model $obj
     */
    public function before_insert(\Orm\Model $obj)
    {
        $primary_key = $obj->primary_key();

        // Iterate all properties.
        foreach($obj->properties() as $field => $props)
        {
            // Find null defaults and set it to _default_value for non-nullable fields
            if(!\Arr::get($props, 'null', false) &&
               \Arr::get($props, 'default', null) === null
               && $obj->{$field} === null && !in_array($field, $primary_key))
            {
                $obj->{$field} = $this->_default_value;
            }
        }
    }
}

// End of file createdat.php
