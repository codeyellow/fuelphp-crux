<?php
namespace Crux\Exception;
/**
 *  Exception to throw when validation failed
 */
class Validation extends \Exception
{
	/**
	 * @var Error array.
	 */
	protected $error = array(
		''
	);

	/**
	 * Overridden \FuelException construct to add a Fieldset instance into the exception
	 *
	 * @param  string  the error message
	 * @param  int  the error code
	 * @param  \Exception any previous exception
	 * @param  \Fieldset  the fieldset on which this exception was triggered
	 */
	public function __construct($message = null, $code = 0, \Exception $previous = null, array $error = array())
	{
		parent::__construct($message, $code, $previous);

		$this->error = $error;
	}

	/**
	 * Gets the Fieldset from this exception
	 *
	 * @return Fieldset
	 */
	public function getError()
	{
		return $this->error;
	}
}