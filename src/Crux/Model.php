<?php
namespace Crux;

class Model extends \Orm\Model
{
    protected static $_observers = array(
        'Orm\\Observer_CreatedAt' => array(
            'events' => array(
                'before_insert' 
            ),
            'mysql_timestamp' => false 
        ),
        'Orm\\Observer_UpdatedAt' => array(
            'events' => array(
                'before_save' 
            ),
            'mysql_timestamp' => false 
        ),
        'Crux\\Observer_DefaultValue' => array(
            'events' => array(
                'before_insert' 
            ) 
        ),
        'Crux\\Observer_Purify' => array(
            'events' => array(
                'before_save' 
            ) 
        ),
        'Orm\\Observer_Validation' => array(
            'events' => array(
                'before_save' 
            ) 
        )
    );

    protected static $properties_cache = array();

    public function __construct(array $data = array(), $new = true, $view = null) 
    {
        parent::__construct($data, $new, $view);

        // Add related objects if given.
        if($new) {
            foreach($this->relations() as $name => $relation) {
                if(array_key_exists($name, $data) && $data[$name] instanceof $relation->model_to) {
                    $this->{$name} = $data[$name];
                }
            } 
        }
    }

    public function __toString()
    {
        return 'hoi';
    }

    public static function exists($id) 
    {
        if (!is_numeric($id)) {
            throw new \InvalidArgumentException();
        }

        return count(\DB::select('*')
        ->from(static::table())
        ->where('id', $id)
        ->execute()) > 0;
    }

    public function resolve($name, array $params = array()) 
    {
        // Check relations.
        $relations = static::relations();
        $class = '';

        if(array_key_exists($name, $relations)) {
            $class = $relations[$name]->model_to;
        }
        
        switch(true) {
            case \Arr::get($params, $name, null) instanceof $class:
                return \Arr::get($params, $name, null);
                break;
            case $class::find(\Arr::get($params, $name . '_id', 0)) instanceof $class:
                return $class::find(\Arr::get($params, $name . '_id', 0));
                break;
        }

        throw new \Exception('Unresolved service');
    }

    /**
     * Search.
     */
    public static function restGetCollection(array $params = array())
    {
        $result = \DB::select('*')->from(static::table())->execute();

        return array(
            'data' => $result->as_array(),
            'totalRecords' => count($result)
        );
    }

    /**
     * Craft a new model.
     *
     * @param array $props
     * @return array
     */
    public static function restPostModel(array $props)
    {
        $id = static::craft($props)->id;
        static::clearCache();
        return static::restGetModel($id);
    }

    /**
     * Get model from database.
     *
     * @return array
     */
    public static function restGetModel($id) {
        $result = \DB::select('*')->from(static::table())->where('id', $id)->execute()->as_array();

        if(count($result) > 0) {
            return $result[0];
        } else {
            throw new \Orm\RecordNotFound('Record not found');
        }
    }

    /**
     * Modify.
     *
     * @param int $id
     * @param array $props
     * @return Model
     */
    public static function restPutModel($id, array $params)
    {
        $model = static::find($id);
        $data = array();
        
        if ($model) {
            $model->modify($params);
            static::clearCache();
            return static::restGetModel($id);
        } else {
            throw new \Orm\RecordNotFound('Record not found');
        }
    }

    /**
     * Modify.
     *
     * @param int $id
     * @param array $props
     * @return Model
     */
    public static function restPatchModel($id, array $params)
    {
       return static::restPutModel($id, $params);
    }

    /**
     * Craft.
     *
     * @param array $props
     * @return bool
     */
    public static function craft(array $params) {
        $model = static::forge($params);
        
        $model->save();

        return $model;
    }

    /**
     * Modify. Filters data so it includes only my properties.
     *
     * @param array $props
     * @return bool
     */
    public function modify(array $params) {
        $data = array();
        unset($params['id']);

        foreach (array_keys(static::properties()) as $property) {
            if (!is_array($property) && isset($params[$property]) && !is_array($params[$property])) {
                $data[$property] = $params[$property];
            }
        }
        
        $this->from_array($data);
        $changes = $this->changedProperties();

        $this->save();

        return $changes;
    }

    /**
     * Erase a model.
     *
     * @param int $id
     * @return boolean
     */
    public static function restDeleteModel($id = null)
    {
        if ($model = static::find($id)) {
            return $model->erase();
        } else {
            throw new \Orm\RecordNotFound('Record not found');
        }
    }

    public function erase() 
    {
        $this->delete();
    }

    
    /**
     * Add Crux observers. TODO: Cache merged crux observers.
     *
     * @return array
     */
    public static function observers($specific = null, $default = null)
    {
        $class = get_called_class();
        $observers = parent::observers($specific, $default);
        $cruxObservers = array();
        
        if (property_exists($class, 'cruxObservers')) {
            $cruxObservers = static::$cruxObservers;
        }
        
        return $cruxObservers + $observers;
    }

    /**
     * @return array;
     */
    public static function properties()
    {
        $class = get_called_class();

        if (array_key_exists($class, static::$properties_cache)) {
            return static::$properties_cache[$class];
        }

        $properties = parent::properties();
        $cruxProperties = array();
        
        if (property_exists($class, 'cruxProperties')) {
            $cruxProperties = static::$cruxProperties;
        }

        $merged = static::autoValidationRules(\Arr::merge($properties, $cruxProperties));
        static::$properties_cache[$class] = $merged;
        return $merged;
    }

    /**
     * Automatically add validation rules from properties.
     *
     * @param array $properties
     * @return array
     */
    protected static function autoValidationRules(array $properties)
    {
        foreach ($properties as $name => &$data) {
            // Create validation array if not set.
            !array_key_exists('validation', $data) && $data['validation'] = array();

            // Set string_like if not set.
            !in_array('string_like', $data['validation']) && array_unshift($data['validation'], 'string_like');

            // Set max length if not set.
            if (!isset($data['validation']['max_length']) && isset($data['character_maximum_length'])) {
                $data['validation']['max_length'] = array(
                    $data['character_maximum_length'] 
                );
            }
        }
        
        return $properties;
    }
    
    public function changedProperties() {
        return empty($this->_original) ? $this->_data : array_diff_assoc($this->_original, $this->_data);
    }
    
    

    /**
     * Returns table fields with prefix.
     *
     * @return array Table fields.
     */
    public static function fieldsPrefixed() {
        $properties = static::fields();

        foreach($properties as &$property){
            $property = static::table() . '.' . $property;
        }

        return $properties;
    }

    /**
     * Returns table fields with prefix.
     *
     * @return array Table fields.
     */
    public static function fieldsPrefixedForSelect() {
        $properties = static::fieldsPrefixed();

        foreach($properties as &$property){
            $property = array($property, str_replace('.', ':', $property));
        }

        return $properties;
    }

    /**
     * Get values from properties.
     *
     * @return array
     */
    public function values() {
        $values = array();

        foreach($this->fields() as $field) {
            $values[$field] = $this->{$field};    
        }
        
        return $values;
    }


    public static function fields() {
        return array_keys(static::properties());
    }

    public function belongs_to() {
        return $this::$_belongs_to;
    }

    public function getCruxObservers()
    {
        return isset($this->cruxObservers) ? $this->cruxObservers : array();
    }

    public static function getObservers()
    {
        return static::$_observers;
    }

    /**
     * Get the class's relations including inheritance.
     *
     * @param   string
     * @return  array
     */
    public static function relations($specific = false)
    {
        $class = get_called_class();
        $parent = get_parent_class($class);

        foreach(static::$_valid_relations as $rel_name => $rel_class)
        {
            $relProperty =  '_'.$rel_name;

            if (
                property_exists($class, '_'.$rel_name) &&
                property_exists($parent, '_'.$rel_name)
            )
            {
                // properties exist, so just merge them. If all elements already
                // exist, then the result will be the same.
                static::${'_'.$rel_name} = \Arr::merge($parent::${'_'.$rel_name}, $class::${'_'.$rel_name});
            }
        }

        return parent::relations($specific);
    }

    /**
     * Add paging clause.
     *
     * @param $query Database query object that supports limit / offset.
     * @param array $params Array of params with limit / offset keys.
     * @return True if limit/offset clause added, false otherwise.
     */
    public static function queryPaging($query, $params)
    {
        if (
            isset($params['limit'], $params['offset']) &&
            is_numeric($params['limit']) && 
            is_numeric($params['offset']) &&
            $params['limit'] > 0 &&
            $params['offset'] >= 0
        ) {
            $query->limit($params['limit']);
            $query->offset($params['offset']);

            return true;
        }

        return false;
    }

    /**
     * Add order by clause.
     */
    public static function queryOrderBy($query, $params)
    {
        // Order.
        if (isset($params['order_by']) && is_array($params['order_by'])) {
            foreach ($params['order_by'] as $field => $direction) {
                if (in_array($direction, array('asc', 'desc'))) {
                    $query->order_by($field, $direction);
                }
            }
        }
    }

    /**
     * Clear cache.
     *
     * @param string $class Clear specific class cache.
     */ 
    public static function clearCache($class = null)
    {
        if (array_key_exists($class, static::$_cached_objects)) {
            static::$_cached_objects[$class] = array();
        } else {
            static::$_cached_objects = array();
        }
    }
}
