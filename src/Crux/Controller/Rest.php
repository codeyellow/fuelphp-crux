<?php
namespace Crux;

/**
 * Default model for Backbone based apps.
 *
 * @author AB Zainuddin
 */
class Controller_Rest extends \Fuel\Core\Controller_Rest
{
    protected $modelName = '';

    /**
     * Set format for output. This can be json, xml ... Very usefull for unit
     * test.
     * 
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * Guess model name on controller start.
     */
    public function before()
    {
        parent::before();
        
        if (strlen($this->modelName) == 0) {
            $this->modelName = $this->guessModelName();
        }
    }

    /**
     *  Catch all the errors, and return the formatted exception
     */
    public function router($resource,$arguments){
        try{
            parent::router($resource,$arguments);
        } catch (\Exception $e) {
            return $this->formatError($e);
        }
    }

    /**
     * Returns the error to the browser.
     * If 500 error -> send a mail.
     *
     * @param \Exception $e
     * @return \Response
     */

    protected function formatError(\Exception $e) {
        $result = array(
            'code' => 0,
            'body' => array()
        );        

        switch (true) {
            case $e instanceof \Orm\ValidationFailed:
                $result['code'] = 1;
                $result['body'] = array();
                
                foreach ($e->get_fieldset()->validation()->error() as $field => $error) {
                    array_key_exists($field, $result['body']) ? $result['body'][$field][] = $error->__toString() : $result['body'][$field][] = $error->__toString();
                }

                return $this->response($result, \Response::status('Bad Request'));
            case $e instanceof \Orm\RecordNotFound:
                $result['code'] = 2;
                return $this->response($result, \Response::status('Bad Request'));
            case $e instanceof \InvalidArgumentException:
                $result['code'] = 3;
                return $this->response($result, \Response::status('Bad Request'));
            default:
                try {
                    // Send a mail to inform the site admin that something is really wrong.
                    $server = \Input::server('SERVER_NAME');

                    if (\Fuel::$env !== \Fuel::DEVELOPMENT) {
                        \Package::load('email');
                        $email = \Email::forge('bug');
                        $email->from('bug@' . \Input::server('SERVER_NAME'));
                        $email->to(\Config::get('email_bugs_to') ?: 'bugz@codeyellow.nl');
                        $email->subject('Bug at ' . \Input::server('SERVER_NAME'));
                        $email->body(
                            'Error: ' . print_r($e,true) . 
                            "\n Session:". print_r(\Session::get(),true) . 
                            "\n Input:". print_r(\Input::all(),true)
                        );
                        $email->send();
                    } 
                } catch (\Exception $f) {
                    \Log::warning($f->getMessage());
                }

                \Log::warning($e->getMessage());

                // Just copy message to body.
                $result['body'] = $e->getMessage();
                return $this->response($result, \Response::status('Internal Server Error'));
        }
    }
    
    /**
     * Guess model name based on controller name.
     */
    protected function guessModelName()
    {
        $potentials = array();
        $namespace = '';
        $exploded = explode('_', get_class($this));
        $excluded = array(
            'controller',
            'api' 
        );
        
        foreach ($exploded as $val) {
            switch(true){
                case strpos($val, '\\') !== false:
                    // Namespace.
                    $namespace .= substr($val, 0, strpos($val, '\\') + 1);
                    break;
                case !in_arrayi($val, $excluded):
                    // Potentials.
                    $potentials[] = $val;
                    break;
            }
        }
        
        return $namespace . 'Model_' . implode('_', $potentials);
    }

    /**
     * Get model name,
     */
    protected function getModelName()
    {
        return $this->modelName;
    }

    /**
     * Get collection.
     *
     * @return \Response
     */
    public function get_collection()
    {
        try {
            $model = $this->getModelName();

            return $this->response($model::restGetCollection(\Input::get()), \Response::status('OK'));
        } catch (\Exception $e) {
            return $this->formatError($e);
        }
    }
    
    /**
     * Get model.
     *
     * @return \Response
     */
    public function get_model($id = 0)
    {
        try {
            $model = $this->getModelName();

            return $this->response($model::restGetModel($id), \Response::status('OK'));
        } catch (\Exception $e) {
            return $this->formatError($e);
        }
        
    }

    /**
     * Craft a new post.
     *
     * @return \Response
     */
    public function post_model()
    {
        try{
            $model = $this->getModelName();
            
            return $this->response($model::restPostModel(\Input::post()), \Response::status('Created') );
        } catch (\Exception $e) {
            return $this->formatError($e);
        }
    }

    /**
     * Erase a post.
     *
     * @param int $id  
     * @return \Response          
     */
    public function delete_model($id = 0)
    {
        try {
            $modelName = $this->getModelName();
        
            return $this->response($modelName::restDeleteModel($id), \Response::status('OK'));   
        } catch (Exception $e) {
            return $this->formatError($e);
        }
    }

    /**
     * Modify a post
     *
     * @param int $id Post id to modify
     * @return \Response
     */
    public function put_model($id = 0)
    {
        try{
            $modelName = $this->getModelName();
        
            return $this->response($modelName::restPutModel($id, \Input::put()), \Response::status('OK'));
        } catch (\Exception $e) {
            return $this->formatError($e);
        }
    }

    /**
     * Modify a post
     *
     * @param int $id Post id to modify
     * @return \Response
     */
    public function patch_model($id = 0)
    {
        try{
            $modelName = $this->getModelName();
        
            return $this->response($modelName::restPatchModel($id, \Input::patch()), \Response::status('OK'));
        } catch (\Exception $e) {
            return $this->formatError($e);
        }
    }
}

